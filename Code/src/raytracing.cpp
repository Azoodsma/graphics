#include <stdio.h>
#ifdef __APPLE__
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#endif
#ifdef WIN32
#include <windows.h>
#endif
#include "raytracing.h"


//temporary variables
//these are only used to illustrate 
//a simple debug drawing. A ray 
Vec3Df testRayOrigin;
Vec3Df testRayDestination;

//the bounding box
Vec3Df boxMin;
Vec3Df boxMax;
Triangle tr1, tr2, tr3, tr4, tr5, tr6, tr7, tr8, tr9, tr10, tr11, tr12;

//use this function for any preprocessing of the mesh.
void init()
{
	//load the mesh file
	//please realize that not all OBJ files will successfully load.
	//Nonetheless, if they come from Blender, they should, if they 
	//are exported as WavefrontOBJ.
	//PLEASE ADAPT THE LINE BELOW TO THE FULL PATH OF THE dodgeColorTest.obj
	//model, e.g., "C:/temp/myData/GraphicsIsFun/dodgeColorTest.obj", 
	//otherwise the application will not load properly
	//plek waar we nieuwe meshes inladen
    MyMesh.loadMesh("dodgeColorTest.obj", true);
	MyMesh.computeVertexNormals();

	boxMin = Vec3Df(INFINITY, INFINITY, INFINITY);
	boxMax = Vec3Df(-INFINITY, -INFINITY, -INFINITY);
	
	//create the points for the bounding box
	for (int i = 0; i < MyMesh.vertices.size(); i++) {
		for (int j = 0; j < 3; j++) {
			if (MyMesh.vertices[i].p[j] < boxMin[j]) boxMin[j] = MyMesh.vertices[i].p[j];
			if (MyMesh.vertices[i].p[j] > boxMax[j]) boxMax[j] = MyMesh.vertices[i].p[j];
		}
	}	

	//one first move: initialize the first light source
	//at least ONE light source has to be in the scene!!!
	//here, we set it to the current location of the camera
	MyLightPositions.push_back(MyCameraPosition);
}
bool isInBoundingBox(const Vec3Df & Vmin, const Vec3Df & Vmax, const Vec3Df & rayO, const Vec3Df rayD) {
	// using slabs method
	float min = -INFINITY;
	float max = INFINITY;

	// compute directionInv  for optmization
	float invX = 1 / rayD[0];
	float invY = 1 / rayD[1];
	float invZ = 1 / rayD[2];

	// instead of swap take this as index position for boxMin or boxMax
	int x = (invX < 0);
	int y = (invY < 0);
	int z = (invZ < 0);


	// compute intersect (boxVertex.x - rayOrigin.x) / rayDirect.x
	min = (Vmin[x] - rayO[0]) * invX;
	max = (Vmax[1 - x] - rayO[0]) * invX;
	float y0 = (Vmin[y] - rayO[1]) * invY;
	float y1 = (Vmax[1 - y] - rayO[1]) * invY;
	// check for false condition
	if ((min > y1) || (y0 > max)) { return false; }


	// else to skip this block incase above condition holds
	else {
		float z0 = (Vmin[z] - rayO[2]) * invZ;
		float z1 = (Vmax[z - 1] - rayO[2]) * invZ;


			// compute extreme min and max value
			min = ((min) > (y0)) ? (min) : (y0);
			max = ((max) < (y1)) ? (max) : (y1);

			//check intersect if does not hold return true
			if ((min > z1) || (z0 > max)) { return false; }

			//min = ((min) > (z0)) ? (min) : (z0);
			//max = ((max) < (z1)) ? (max) : (z1);
			return true;
		
		}
	}

float rayIntersectTriangle(const Vec3Df & origin, Vec3Df direction, int i) {

	const float EPS = 0.000001;
	
	Vec3Df tv0 = MyMesh.vertices[MyMesh.triangles[i].v[0]].p;
	Vec3Df tv1 = MyMesh.vertices[MyMesh.triangles[i].v[1]].p;
	Vec3Df tv2 = MyMesh.vertices[MyMesh.triangles[i].v[2]].p;

	Vec3Df edge1 = tv1 - tv0;
	Vec3Df edge2 = tv2 - tv0;

	Vec3Df h = Vec3Df::crossProduct(direction, edge2);
	float a = Vec3Df::dotProduct(edge1, h);

	if (a > -EPS && a < EPS)
		return -1;

	float f = 1 / a;
	Vec3Df s = origin - tv0;
	float u = f * (Vec3Df::dotProduct(s, h));

	if (u < 0.0 || u > 1.0)
		return -1;

	Vec3Df q = Vec3Df::crossProduct(s, edge1);
	float v = f * Vec3Df::dotProduct(direction, q);

	if (v < 0.0 || u + v > 1.0)
		return -1;

	float t = f * Vec3Df::dotProduct(edge2, q);

	return t;
}

Vec3Df diffuseOnly(const Vec3Df & vertexPos, Vec3Df & normal, const Vec3Df & lightPos, Vec3Df Kd)
{
	Vec3Df lightdir = lightPos - vertexPos;
	lightdir.normalize();
	return Kd * Vec3Df::dotProduct(normal, lightdir);
}

bool isInLight(Vec3Df pos) {
	Vec3Df direction = MyLightPositions[0] - pos;
	direction.normalize();

	float closestDist = INFINITY;
	int closestTriangle;

	for (int i = 0; i < MyMesh.triangles.size(); i++) {
		float dist = rayIntersectTriangle(pos, direction, i);
		if (dist > 0.000001 && closestDist>dist) {
			return false;
		}
	}
	return true;
}

//return the color of your pixel.
Vec3Df performRayTracing(const Vec3Df & origin, const Vec3Df & dest)
{
	Vec3Df direction = dest - origin;
	direction.normalize();

	float closestDist = INFINITY;
	int closestTriangle;

	//just return 0,0,0 if not in the bounding box
	if (!isInBoundingBox(boxMin, boxMax, origin, direction))
		return Vec3Df(0, 0, 0);

	for (int i = 0; i < MyMesh.triangles.size(); i++) {
		float dist = rayIntersectTriangle(origin, direction, i);
		if (dist > 0 && closestDist>dist) {
			closestDist = dist;
			closestTriangle = i;
		}
	}

	if (closestDist != INFINITY && isInLight(closestDist*direction + origin)){
		Vec3Df kd = MyMesh.materials[MyMesh.triangleMaterials[closestTriangle]].Kd();

		Vec3Df tv0 = MyMesh.vertices[MyMesh.triangles[closestTriangle].v[0]].p;
		Vec3Df tv1 = MyMesh.vertices[MyMesh.triangles[closestTriangle].v[1]].p;
		Vec3Df tv2 = MyMesh.vertices[MyMesh.triangles[closestTriangle].v[2]].p;

		Vec3Df edge1 = tv1 - tv0;
		Vec3Df edge2 = tv2 - tv0;

		Vec3Df normal = Vec3Df::crossProduct(edge1, edge2);
		normal.normalize();
		//return kd;
		return diffuseOnly(closestDist*direction+origin,normal, MyLightPositions[0],kd);
	}

	return Vec3Df(0,0,0);
}



void yourDebugDraw()
{
	//draw open gl debug stuff
	//this function is called every frame

	//let's draw the mesh
	MyMesh.draw();
	
	//let's draw the lights in the scene as points
	glPushAttrib(GL_ALL_ATTRIB_BITS); //store all GL attributes
	glDisable(GL_LIGHTING);
	glColor3f(1,1,1);
	glPointSize(10);
	glBegin(GL_POINTS);
	for (int i=0;i<MyLightPositions.size();++i)
		glVertex3fv(MyLightPositions[i].pointer());
	glEnd();
	glPopAttrib();//restore all GL attributes
	//The Attrib commands maintain the state. 
	//e.g., even though inside the two calls, we set
	//the color to white, it will be reset to the previous 
	//state after the pop.


	//as an example: we draw the test ray, which is set by the keyboard function
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glColor3f(0,1,1);
	glVertex3f(testRayOrigin[0], testRayOrigin[1], testRayOrigin[2]);
	glColor3f(0,0,1);
	glVertex3f(testRayDestination[0], testRayDestination[1], testRayDestination[2]);
	glEnd();
	glPointSize(10);
	glBegin(GL_POINTS);
	glVertex3fv(MyLightPositions[0].pointer());
	glEnd();
	glPopAttrib();
	
	//draw whatever else you want...
	////glutSolidSphere(1,10,10);
	////allows you to draw a sphere at the origin.
	////using a glTranslate, it can be shifted to whereever you want
	////if you produce a sphere renderer, this 
	////triangulated sphere is nice for the preview
}


//yourKeyboardFunc is used to deal with keyboard input.
//t is the character that was pressed
//x,y is the mouse position in pixels
//rayOrigin, rayDestination is the ray that is going in the view direction UNDERNEATH your mouse position.
//
//A few keys are already reserved: 
//'L' adds a light positioned at the camera location to the MyLightPositions vector
//'l' modifies the last added light to the current 
//    camera position (by default, there is only one light, so move it with l)
//    ATTENTION These lights do NOT affect the real-time rendering. 
//    You should use them for the raytracing.
//'r' calls the function performRaytracing on EVERY pixel, using the correct associated ray. 
//    It then stores the result in an image "result.bmp".
//    Initially, this function is fast (performRaytracing simply returns 
//    the target of the ray - see the code above), but once you replaced 
//    this function and raytracing is in place, it might take a 
//    while to complete...
void yourKeyboardFunc(char t, int x, int y, const Vec3Df & rayOrigin, const Vec3Df & rayDestination)
{

	//here, as an example, I use the ray to fill in the values for my upper global ray variable
	//I use these variables in the debugDraw function to draw the corresponding ray.
	//try it: Press a key, move the camera, see the ray that was launched as a line.
	testRayOrigin=rayOrigin;	
	testRayDestination=rayDestination;
	
	// do here, whatever you want with the keyboard input t.
	
	//...
	
	
	std::cout<<t<<" pressed! The mouse was in location "<<x<<","<<y<<"!"<<std::endl;	
}
